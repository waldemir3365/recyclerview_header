package br.com.testeheader;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HeaderViewHolder extends RecyclerView.ViewHolder {

    public TextView headerTitle;
    public Button btn_Teste;

    public HeaderViewHolder( View itemView) {
        super(itemView);

        headerTitle = (TextView)itemView.findViewById(R.id.header_id);
        btn_Teste = itemView.findViewById(R.id.btn_teste);
    }



}

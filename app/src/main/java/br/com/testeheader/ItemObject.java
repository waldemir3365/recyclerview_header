package br.com.testeheader;

public class ItemObject {

    private String contents;

    public ItemObject(String contents) {
        this.contents = contents;
    }

    public String getContents() {
        return contents;
    }
}

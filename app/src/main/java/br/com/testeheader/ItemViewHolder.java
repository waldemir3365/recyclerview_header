package br.com.testeheader;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class ItemViewHolder extends RecyclerView.ViewHolder{

    public TextView itemContent;

    public ItemViewHolder(@NonNull View itemView) {
        super(itemView);

        itemContent = (TextView)itemView.findViewById(R.id.item_content);
    }
}

package br.com.testeheader;

import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView addHeaderRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //instanciate
        addHeaderRecyclerView = (RecyclerView) findViewById(R.id.add_header);

        //set Manager Layout
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        addHeaderRecyclerView.setLayoutManager(linearLayoutManager);

        //performace list
        addHeaderRecyclerView.setHasFixedSize(true);


        OnItemClickListener listener = new OnItemClickListener() {
            @Override
            public void onClick() {

                Toast.makeText(getBaseContext(), "Eu sou a lenda ", Toast.LENGTH_SHORT).show();
            }
        };

        //instanciate adapter
        CustomRecyclerViewAdapater customAdapter = new CustomRecyclerViewAdapater(getDataSource(), this, listener);

        //set adapter
        addHeaderRecyclerView.setAdapter(customAdapter);


    }

    private List<ItemObject> getDataSource(){
        List<ItemObject> data = new ArrayList<ItemObject>();
        data.add(new ItemObject("First Header"));
        data.add(new ItemObject("This is the item content in the first position"));
        data.add(new ItemObject("This is the item content in the second position"));
        data.add(new ItemObject("This is the item content in the third position"));
        data.add(new ItemObject("This is the item content in the fourth position"));
        data.add(new ItemObject("This is the item content in the fifth position"));
        data.add(new ItemObject("This is the item content in the first position"));
        data.add(new ItemObject("This is the item content in the second position"));
        data.add(new ItemObject("This is the item content in the third position"));
        data.add(new ItemObject("This is the item content in the fourth position"));
        data.add(new ItemObject("This is the item content in the fifth position"));
        return data;
    }
}

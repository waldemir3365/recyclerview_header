package br.com.testeheader;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;


public class CustomRecyclerViewAdapater extends RecyclerView.Adapter<RecyclerView.ViewHolder>{


    private static final String TAG = CustomRecyclerViewAdapater.class.getSimpleName();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<ItemObject> itemObjects;
    private OnItemClickListener listener;

    public CustomRecyclerViewAdapater(List<ItemObject> itemObjects, Context context, OnItemClickListener listener) {
        this.itemObjects = itemObjects;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup viewGroup, int viewType) {

        if (viewType == TYPE_HEADER) {
            View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_layout, viewGroup, false);
            return new HeaderViewHolder(layoutView);

        } else if (viewType == TYPE_ITEM) {

            View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_layout, viewGroup, false);
            return new ItemViewHolder(layoutView);
        }

        throw new RuntimeException("No match for " + viewType + ".");
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder viewHolder, int position) {

        ItemObject mObject = itemObjects.get(position);



        if(viewHolder instanceof HeaderViewHolder){

            ((HeaderViewHolder) viewHolder).headerTitle.setText(mObject.getContents());

            ((HeaderViewHolder) viewHolder).btn_Teste.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick();
                }
            });

        }else if(viewHolder instanceof ItemViewHolder){

            ((ItemViewHolder) viewHolder).itemContent.setText(mObject.getContents());
        }

    }

    private ItemObject getItem(int position) {
        return itemObjects.get(position);
    }

    @Override
    public int getItemCount() {
        return itemObjects.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }


}
